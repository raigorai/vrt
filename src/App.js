import React from "react";
import logo from "./logo.svg";
import "./App.css";
import PrimaryButton from "./components/atoms/PrimaryButton";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <PrimaryButton>Learn React</PrimaryButton>
      </header>
    </div>
  );
}

export default App;
