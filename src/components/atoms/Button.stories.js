import React from "react";
import { storiesOf } from "@storybook/react";
import { PrimaryButton, SecondaryButton } from "./Button";

storiesOf("Atoms/Button", module).add("Button", () => (
  <>
    <PrimaryButton>Primary</PrimaryButton>
    <SecondaryButton>Secondary</SecondaryButton>
  </>
));
