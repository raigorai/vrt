import { configure, addDecorator, addParameters } from "@storybook/react";
import { withScreenshot } from "storycap";

addDecorator(withScreenshot);
addParameters({
  screenshot: {
    viewports: {
      large: {
        width: 1024,
        height: 768
      },
      small: {
        width: 375,
        height: 668
      },
      xsmall: {
        width: 320,
        height: 568
      }
    },
    delay: 200
  }
});

// Load stories
const req = require.context("../src", true, /\.stories\.js$/);

configure(() => {
  req.keys().forEach(filename => req(filename));
}, module);
